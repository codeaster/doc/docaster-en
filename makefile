SHELL = bash

# '_build' is used in some rst files for raw include
BUILDDIR ?= _build
BUILDER ?= sif
OPTIONS ?=
SIF = rst2sphinx.sif

URL_EDF = https://minio.retd.edf.fr/codeaster/sif/lab/$(SIF)
URL_PUB = https://www.code-aster.org/FICHIERS/singularity/$(SIF)

default: html

sif: .sif/$(SIF)
venv: .venv
native: check_requirements

check_requirements:
	@python3 -c "import sphinx, sys ; sys.exit(not ((6, 0) < sphinx.version_info[:2] < (7, 0)))" || \
		(echo "Sphinx >6,<7 is required. You should use BUILDER=sif" ; exit 1)
	@python3 -c "import sphobjinv" || \
		(echo "sphobjinv is required. You should use BUILDER=sif" ; exit 1)

.venv:
	@echo "+ creating virtualenv..."
	@python3 -c "import sys ; sys.exit(not (sys.version_info[:2] >= (3, 8)))" || \
		(echo "Python >= 3.8 is required. You should use BUILDER=sif" ; exit 1)
	@python3 -m venv --system-site-packages --prompt 'venv' .venv
	@( \
		. .venv/bin/activate ; \
		python3 -m pip install -r data/requirements.txt ; \
		deactivate ; \
	)

.sif/$(SIF):
	@mkdir -p .sif
	@echo "+ checking for image provider..."
	@[ -z "$${SINGULARITY_NAME}" ] || (echo "++ already in a container, try with: BUILDER=venv" ; exit 1)
	@( \
		printf "++ checking for access to EDF server... " ; \
		wget --no-check-certificate --timeout=3 --tries=1 --no-verbose --quiet -O /dev/null https://gitlab.pleiade.edf.fr/ ; \
		iret=$$? ; \
		url=$(URL_EDF) ; \
		if [ $${iret} -ne 0 ]; then \
			echo "no" ; \
			url=$(URL_PUB) ; \
		else \
			echo "yes" ; \
		fi ; \
		echo "+ download singularity image from $${url}..." ; \
		wget --no-check-certificate --timeout=3 --tries=1 -O $@ $${url} ; \
	)
	@chmod 755 $@

builder: $(BUILDER)

html: $(BUILDER)
	@echo "+ running sphinx..."
	@mkdir -p $(BUILDDIR)
	@( \
		export GITREV=$$(git rev-parse HEAD) ; \
		cmd=( python3 data/docaster_build.py "--builddir=$(BUILDDIR)" $(OPTIONS) "$(PATTERN)" ) ; \
		if [ $(BUILDER) = "native" ]; then \
			echo "++ running sphinx natively..." ; \
			"$${cmd[@]}" ; \
		elif [ $(BUILDER) = "sif" ]; then \
			echo "++ running sphinx using $(SIF)..." ; \
			singularity exec --bind /etc/ssl/certs --bind $(PWD) --bind $(abspath $(BUILDDIR)) .sif/$(SIF) "$${cmd[@]}" ; \
		else \
			. .venv/bin/activate ; \
			echo "++ running sphinx from virtualenv..." ; \
			"$${cmd[@]}" ; iret=$$? ; \
			deactivate ; \
			[ $${iret} -eq 0 ] || false ; \
		fi ; \
	)

clean:
	@echo "+ cleaning cache directory '_cache'..."
	@rm -rf _cache
	@echo "+ cleaning build directory '$(BUILDDIR)'..."
	@rm -rf $(BUILDDIR)
	@echo "+ removing empty directories from 'source/'..."
	@rmdir -p $$(find source -type d -empty) 2> /dev/null || true
	@mkdir -p source/intranet

clean-sif:
	@echo "+ cleaning singularity image..."
	@rm -rf .sif

clean-venv:
	@echo "+ cleaning virtualenv..."
	@rm -rf .venv

clean-native:

clean-builder: clean-$(BUILDER)

distclean: clean-builder clean

.PHONY: html clean clean-sif clean-venv distclean
.PHONY: builder check_requirements venv sif native
