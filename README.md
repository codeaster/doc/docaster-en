# code_aster documentation

The repository provides an automatic translation of the documentation of code_aster.

**The RST files should not be edited. They are the result of the machine translation process.**

## Build the html pages

The html pages are built using Sphinx from the RST files.

You can build all the documentation with auto-generated pages in english with:

```bash
$ ./doc_build.sh --lang=en html
```


See [docaster][1] for more details.

[1]: ../../../../docaster
