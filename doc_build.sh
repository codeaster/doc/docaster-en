#!/bin/bash

set_prefix() {
    local this=$(readlink -n -f "$1")
    prefix=$(dirname "${this}")
}

set_prefix "${0}"

usage()
{
    echo "usage: ${0} [options] command"
    echo
    echo "  This script provides a simple access to the most common tasks on the documentation."
    echo
    echo "command:"
    echo "  html                build html pages"
    echo "  changed (default)   only build html pages for changed documents since --from (see below)"
    echo "  open                open the index page with your default browser"
    echo "  clean               remove the existing build directory"
    echo "  clean-builder       remove the builder directory"
    echo "  distclean           clean + clean-builder"
    echo "  sync                update repository (and also intranet documents if available, *EDF* only)"
    echo "  create docid        create a new document"
    echo
    echo "options for 'html', 'changed', 'open', 'clean', 'distclean':"
    echo "  --builddir=DIR      alternative build directory (default: _build)"
    echo "  --builder=BUILDER   one of 'sif' (default), 'venv' or 'native'."
    echo "      Can be set using the BUILDER environment variable."
    echo
    echo "options for 'html', 'changed':"
    echo "  --twice             build twice (to update reference links)"
    echo "  --pattern=PATTERN   pattern use as a filter to select docs, start with 're:' for regexp"
    echo "  -j, --jobs          number of jobs in parallel (default: all available cores)"
    echo "  --baseurl=URL       define the final base url of the pages"
    echo "  --nosymlinks        do not use symlinks (increase disk usage)"
    echo "  --iframe            insert menu using an iframe (server required)"
    echo "  --stop-on-failure   stop after one build failed"
    echo "  --lang=LANG         select language for automatically generated pages (default: fr)"
    echo "  --rtd               use a preselected pattern for RTD"
    echo "  -n, --nobuild       only prepare docs structure, do not build docs"
    echo "  -v                  more verbosity (can be repeated)"
    echo "  -q                  less verbosy, just warnings on stderr"
    echo
    echo "options for 'changed':"
    echo "  --from COMMIT       use to build the changed docs since this commit (default: HEAD)"
}

# global variables
HAS_INTRANET=0
INTRA="source/intranet"
GITINTRA="git -C ${INTRA}"
# RUNNING_CI should be set by CI config file to avoid asking confirmations

_error()
{
    echo "ERROR: ${1}"
    exit 1
}

_assert_ok() {
    [ $1 -eq 0 ] || _error "$2, exit code $1"
}

help_and_exit() {
    usage
    [ $# -gt 0 ] && _error "${*}"
    exit 1
}

check_config() {
    remove_submodule
    check_hook "$@"
}

check_hook() {
    [ ${HAS_INTRANET} -eq 0 ] && return
    [ ! -d ${prefix}/.git/hooks ] && return
    local hookc=${prefix}/.git/hooks/pre-commit
    local hookp=${prefix}/.git/hooks/pre-push
    local reinst
    if [ ! -z "$1" ] && [ "$1" = "force" ]; then
        rm -f ${hookc} ${hookp}
        reinst=re
    fi
    if [ ! -f ${hookc} ]; then
        cp -f ${prefix}/data/pre-commit-hook ${hookc}
        chmod 755 ${hookc}
        echo "+ pre-commit hook ${reinst}installed"
    fi
    if [ ! -f ${hookp} ]; then
        cp -f ${prefix}/data/pre-push-hook ${hookp}
        chmod 755 ${hookp}
        echo "+ pre-push hook ${reinst}installed"
    fi
}

init_intranet() {
    if [ ${HAS_INTRANET} -eq 1 ]; then
        return 0
    fi
    printf "+ checking for access to restricted documents server... "
    wget --no-check-certificate --timeout=3 --tries=1 --no-verbose --quiet -O /dev/null https://gitlab.pleiade.edf.fr/
    iret=$?
    if [ ${iret} -ne 0 ]; then
        echo "no"
        return -1
    else
        echo "yes"
    fi
    if [ -z "${RUNNING_CI}" ]; then
        printf "+ do you want to clone the repository of intranet documents (Y/n): "
        read answer
        answer=$(sed 's/^ *[yYoO] *$/yes/' <<< "${answer}")
        if [ "${answer}" != "yes" ]; then
            return -1
        fi
    fi

    echo "+ ${INTRA} repository does not already exists"
    local url=$(git config --local --get remote.origin.url)
    url=$(echo ${url} | sed -e 's%/docaster\.git$%/docaster-intranet.git%g' -e 's%/docaster$%/docaster-intranet.git%g')
    echo "+ cloning intranet documents from $url..."
    git clone ${url} ${INTRA}
    local iret=$?
    if [ ${iret} -ne 0 ]; then
        echo "ERROR: can not clone repository"
        exit 1
    fi
    return 0
}

update_repo() {
    local git=git
    local repo="public"
    if [ ! -z "$1" ] && [ "$1" = "intranet" ]; then
        repo="intranet"
        git=${GITINTRA}
    fi
    local iret
    if [ "$1" = "intranet" ]; then
        init_intranet
        iret=$?
        [ ${iret} -eq -1 ] && return 0
        [ ${iret} -eq 0 ] || exit ${iret}
    fi
    echo "+ updating ${repo} documents..."
    ${git} fetch
    iret=$?
    [ ${iret} -eq 0 ] || exit ${iret}
    if [ -z "$(${git} config --local --get branch.main.remote)" ]; then
        ${git} branch --set-upstream-to origin/main main
    fi
    local branch=$(${git} rev-parse --abbrev-ref HEAD)
    local br0="${branch}"
    if [ "${branch}" != "main" ]; then
        echo "+ ${repo}: current branch is '${branch}'"
        if [ -z "${RUNNING_CI}" ]; then
            printf "+ ${repo}: do you want to switch on and update 'main' branch (y/N): "
            read answer
            answer=$(sed 's/^ *[yYoO] *$/yes/' <<< "${answer}")
            if [ "${answer}" = "yes" ]; then
                branch="main"
            fi
        fi
    fi
    if [ "${branch}" = "main" ]; then
        [ "${br0}" != "main" ] && ${git} checkout main
        ${git} pull --ff-only origin main
        iret=$?
        [ ${iret} -eq 0 ] || exit ${iret}
    fi
}

get_changes() {
    # usage: get_changes repopath ref
    repo="$1"
    ref="$2"
    if [ "${repo}" != "${INTRA}" ] || [ ${HAS_INTRANET} -eq 1 ]; then
        base=$(git -C ${repo} merge-base ${ref} HEAD)
        git -C ${repo} diff --name-only ${base}
    fi
}

set_pattern() {
    cat << EOF | python3 - $1
import re
import sys

with open(sys.argv[1]) as fchg:
    files = fchg.read()

expr = re.compile(r"([arduvms]{1,2}[0-9]\.[0-9mk]{2}\.[0-9]{2}[0-9]?)")
keys = set()
for name in files.split():
    mat = expr.search(name)
    if mat:
        keys.add(mat.group(1))
print("|".join(keys))
EOF
}

action_create() {
    echo "+ create a new document"
    if [ -z "$1" ] ; then
        help_and_exit "a document key is expected" >&2
    fi
    dockey="$1"
    egrep -q '^s?[arduvm][0-9]\.[0-9]{2}\.[0-9]{2}[0-9]?$' <<< "${dockey}"
    ok=$?
    if [ ${ok} -ne 0 ]; then
        _error "invalid document key (expecting format 'x0.00.00'): ${dockey}"
    fi
    repo="source/manuals"
    if [ ${HAS_INTRANET} -eq 1 ]; then
        printf "WARNING: Should access to this document be restricted (y/N)? "
        read answer
        answer=$(sed 's/^ *[yYoO] *$/yes/' <<< "${answer}")
        if [ "${answer}" = "yes" ]; then
            repo="${INTRA}"
        fi
    fi
    sect=$(awk -F. '{print $1}' <<< "${dockey}")
    man="man_${sect:0:1}"
    path="${repo}/${man}/${sect}/${dockey}"
    [ -e "${path}" ] && _error "already exists: ${path}"
    mkdir -p $path
    cp ${prefix}/data/templates/create/index.tpl ${path}/index.rst
    cp ${prefix}/data/templates/create/page.tpl ${path}/page.rst
    sed -i -e "s/%dockey%/${dockey}/g" ${path}/index.rst ${path}/page.rst
    echo "+ document created: ${path}"
}

remove_submodule() {
    keys=$(git config --local --list | egrep submodule.source.intranet | awk -F"=" '{print $1}')
    if [ ! -z "${keys}" ]; then
        for key in ${keys}; do
            git config --local --unset ${key}
        done
    fi
}

docaster_main()
{
    local options=()
    local action="html"
    local changed=0
    local refrev
    local twice=0
    local argsinit="$@"
    local remote="origin"
    local lang="fr"
    local key
    # printf "start time: $(date)\n"
    printf "+ command line: $0 $*\n"

    OPTS=$(getopt -o hj:nvqm: \
        --long help,sync,builddir:,builder:,twice,format:,pattern:,jobs:,nosymlinks,nobuild,iframe,keep-sidebar-menu,baseurl:,from:,stop-on-failure,lang:,rtd \
        -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        help_and_exit "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) help_and_exit ;;
            -v | -q | -n | --nobuild | --nosymlinks | --iframe | --rtd | --stop-on-failure | --keep-sidebar-menu ) options+=( "$1" ) ;;
            -j | --jobs ) options+=( "--jobs=$2" ) ; shift ;;
            --lang ) options+=( "--lang=$2" ) ; shift ;;
            --twice ) twice=1 ;;
            --from ) refrev="$2" ; shift ;;
            --baseurl ) options+=( "--baseurl=$2" ) ; shift ;;
            --format ) options+=( "-b" "$2" ) ; shift ;;
            --builddir ) export BUILDDIR="$2" ; shift ;;
            --builder ) export BUILDER="$2" ; shift ;;
            --pattern ) export PATTERN="$2" ; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done
    [ $# -gt 2 ] && help_and_exit "unexpected argument(s): ${@}"
    if [ -e ${INTRA}/.git ]; then
        HAS_INTRANET=1
    fi
    check_config
    if [ $# -eq 0 ]; then
        action="html"
        changed=1
    else
        case "$1" in
            html | sync | open | create | builder | clean | clean-builder | distclean ) action="$1" ;;
            changed ) action="html" ; changed=1 ;;
            help ) help_and_exit ;;
            * ) help_and_exit "unsupported command: $1" ;;
        esac
    fi
    if [ $# -gt 1 ]; then
        case "${action}" in
            "create" ) key="$2" ;;
            * ) help_and_exit "unexpected argument(s): ${@}" ;;
        esac
    fi
    if [ -z "${refrev}" ]; then
        refrev="HEAD"
    fi

    local pattern
    if [ ${changed} -eq 1 ]; then
        printf "+ checking changes since ${refrev}... "
        fchg=$(mktemp tmp.chg.XXXXXXXX)
        (
            get_changes . ${refrev}
            get_changes ${INTRA} ${refrev}
        ) > ${fchg}
        pattern=$( set_pattern ${fchg} )
        rm -f ${fchg}
        if [ -z "${pattern}" ]; then
            if [ ${changed} -eq 1 ]; then
                echo "nothing changed, working tree clean"
                echo "Perhaps you would have wish to run:"
                echo "  $0 $argsinit html"
                echo "Use --help for more options."
                exit 0
            else
                echo "nothing to commit, working tree clean"
                # 'git commit' returns 1 in this case
                exit 1
            fi
        else
            show="${pattern}"
            [ ${#show} -gt 25 ] && show="${show:0:24}…"
            echo "${show}"
            export PATTERN="re:${pattern}"
        fi
    fi
    iret=0
    case "${action}" in
        open )
            xdg-open ${BUILDDIR:-_build}/index.html ;;
        sync )
            check_config force
            update_repo public
            iret=$?
            [ ${iret} -eq 0 ] || exit ${iret}
            update_repo intranet
            iret=$? ;;
        create )
            action_create "${key}";;
        * )
            show="${PATTERN}"
            [ ${#show} -gt 25 ] && show="${show:0:24}…"
            echo make ${action} OPTIONS="${options[@]}" PATTERN="${show}"
            export OPTIONS="${options[@]}"
            time (
                cd ${prefix}
                make ${action}
                kret=$?
                #[ ${kret} -eq 0 ] &&
                if [ ${twice} -eq 1 ]; then
                    make ${action}
                    kret=$?
                fi
                exit ${kret}
            )
            iret=$? ;;
    esac

    # printf "end time: $(date)\n\n"
    return ${iret}
}

docaster_main "${@}"
exit ${?}
