"""
This module defines a glossary used by templates and other generated pages.
"""

# It this glossary grows, prefer use gettext or equivalent.
DEFS = {
    "fr": {
        "CATEGORY_ORDER": {
            "man_u": "Utilisation",
            "man_r": "Référence",
            "man_v": "Validation",
            "man_d": "Descriptif informatique",
            "man_a": "Administration",
            "man_m": "Matériaux",
            "man_s": (
                '<img src="_static/smeca-light.png" class="logo__image only-light" alt="salome_meca"/>'
                '<img src="_static/smeca-dark.png" class="logo__image only-dark" alt="salome_meca"/>'
            ),
        },
        "INTRANET_MARK": '<font color="#ff0000" size="-2">&nbsp;accès restreint&nbsp;</font>',
        "LIST_MENU": "listes",
        "ALL_DOCS": "tous les documents",
        "CMD_BY_NAME": "commandes par nom",
        "TEST_BY_NAME": "tous les cas-tests par nom",
        "TEST_VALID_BY_NAME": "cas-tests de validation par nom",
        "TEST_VERIF_BY_NAME": "cas-tests de vérification par nom",
        "UNAVAIL": "Ce manuel n'est pas encore disponible.",
    },
    "en": {
        "CATEGORY_ORDER": {
            "man_u": "User's Guide",
            "man_r": "Theory",
            "man_v": "Validation",
            "man_d": "Development",
            "man_a": "Quality",
            "man_m": "Material",
            "man_s": (
                '<img src="_static/smeca-light.png" class="logo__image only-light" alt="salome_meca"/>'
                '<img src="_static/smeca-dark.png" class="logo__image only-dark" alt="salome_meca"/>'
            ),
        },
        "INTRANET_MARK": '<font color="#ff0000" size="-2">&nbsp;restricted access&nbsp;</font>',
        "LIST_MENU": "lists",
        "ALL_DOCS": "all documents",
        "CMD_BY_NAME": "commands by name",
        "TEST_BY_NAME": "all testcases by name",
        "TEST_VALID_BY_NAME": "validation testcases by name",
        "TEST_VERIF_BY_NAME": "verification testcases by name",
        "UNAVAIL": "This manual is not yet available.",
    },
}


class Glossary:
    """Object to access to the glossary terms."""

    _def = None

    @classmethod
    def is_supported(cls, lang):
        """Tell if a language is supported"""
        return bool(DEFS.get(lang))

    @classmethod
    def select(cls, lang):
        if not cls.is_supported(lang):
            print(f"i18n: unknown language {lang!r}, use 'fr' glossary")
            lang = "fr"
        cls._def = DEFS.get(lang)
        return cls

    @classmethod
    def gettext(cls, key):
        """Extract a string or an object from the current glossary.

        Arguments:
            key (str): Access key.

        Returns:
            misc: A localized term or object.
        """
        if not cls._def:
            cls.select("?")
        return cls._def[key]
