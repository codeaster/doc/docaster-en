#!/usr/bin/env python

import argparse
import os
import re
import shutil
import sys
import time
from multiprocessing import Value
from multiprocessing.pool import Pool
from pathlib import Path

import sphinx.cmd.build
import sphinx.ext.intersphinx as ISP
import sphobjinv as SOI
from jinja2 import Template

# This limit prevents any program from getting into infinite recursion
# In our case we need to increase this limit due to the number of documents
sys.setrecursionlimit(10000)

DATA = Path(__file__).parent
sys.path.append(str(DATA))

from glossary import Glossary

SOURCE = DATA.parent / "source"

_load_mappings_ref = ISP.load_mappings


def parse_args():
    """Parse command line arguments.

    Returns:
        argparse.Namespace: Container of parsed arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b", dest="format", default="html", choices=("html", "singlehtml"), help="select builder"
    )
    parser.add_argument(
        "-j",
        "--jobs",
        default=os.cpu_count(),
        type=int,
        help="number of jobs in parallel (default: all available cores)",
    )
    parser.add_argument(
        "-n",
        "--nobuild",
        action="store_true",
        help="do not call *sphinx-build*, only prepare docs structure",
    )
    parser.add_argument("--builddir", default="_build", type=Path, help=argparse.SUPPRESS)
    parser.add_argument(
        "--baseurl",
        action="store",
        help="final base url (for intersphinx). Defaults to `builddir`.",
    )
    parser.add_argument(
        "pattern",
        nargs="?",
        default="",
        type=str,
        help="string that may be used to filter the docs to be built.",
    )
    parser.add_argument(
        "--rtd",
        action="store_const",
        const="re:^(u[047]|d8)",
        default="",
        help="use predefined regular expression for RTD build (avoids special "
        "chars in the yaml file).",
    )
    parser.add_argument(
        "--nosymlinks",
        action="store_false",
        dest="symlinks",
        default=True,
        help="do not use symlinks",
    )
    parser.add_argument(
        "--keep-sidebar-menu",
        action="store_false",
        dest="remove_menu",
        default=True,
        help="do not use symlinks",
    )
    parser.add_argument(
        "--iframe",
        action="store_true",
        dest="iframe",
        default=False,
        help="use iframe to include the navigation menu",
    )
    parser.add_argument(
        "--stop-on-failure",
        action="store_true",
        dest="stop_on_failure",
        default=False,
        help="stop building the documents after one has failed",
    )
    parser.add_argument("--lang", default="fr", help="language used for auto-generated pages.")
    return parser.parse_known_args()


def _pool_init(value_global):
    """Initialize the global counter."""
    global counter
    counter = value_global


class Builder:
    """Build the html pages of the documentation.

    Args:
        args (argparse.Namespace): Command line options.
        others (tuple[str]): Additional arguments passed to *sphinx-build*.
    """

    def __init__(self, args, others=()) -> None:
        self.format = args.format
        self.destdir = args.builddir.resolve()
        self.destdir.mkdir(parents=True, exist_ok=True)
        # cachedir will contain html files included using relative path
        self.cachedir = DATA.parent / "_cache"
        self.cachedir.mkdir(parents=True, exist_ok=True)
        self.pattern = args.pattern or args.rtd
        self.baseurl = args.baseurl or str(self.destdir)
        self.symlinks = args.symlinks
        self.remove_menu = args.remove_menu
        self.iframe = args.iframe
        self.stop_on_failure = args.stop_on_failure
        self.sphinx_args = others
        self.lang = args.lang
        if not Glossary.is_supported(args.lang):
            self.lang = "fr"
        self.struct = {}
        self.has_intranet = False

    def raise_error(self, msg):
        """Raise an exception or print a message.

        Arguments:
            msg (str): Text of the message.
        """
        if self.stop_on_failure:
            raise ValueError(msg)
        print(msg)

    def get_doc_structure(self):
        """Generate the structure of the documents."""
        for man_path in self._filtered_docs():
            subdir = man_path.relative_to(SOURCE)
            # print("+++ adding", subdir)
            man = subdir.parts[1]
            doc_name = subdir.name
            special = subdir.parent.name == "other_pages"
            if special:
                sub_category = "0." + Glossary.gettext("LIST_MENU")
                if doc_name == "list":
                    doc_name = "9." + Glossary.gettext("ALL_DOCS")
                elif "commands" in doc_name:
                    doc_name = "1." + Glossary.gettext("CMD_BY_NAME")
                elif "test_cases" in doc_name:
                    doc_name = "0." + Glossary.gettext("TEST_BY_NAME")
                elif "test_verif" in doc_name:
                    doc_name = "1." + Glossary.gettext("TEST_VERIF_BY_NAME")
                elif "test_valid" in doc_name:
                    doc_name = "2." + Glossary.gettext("TEST_VALID_BY_NAME")
                elif doc_name in ("home", "all_documents"):
                    continue
            else:
                # extract the title from index.rst of the document
                title = ""
                intra = "intranet" in man_path.parts
                with open(f"{SOURCE}/{subdir}/index.rst") as f:
                    searched = f"**{doc_name}**"
                    for line in f:
                        if searched in line:
                            title = line.split(maxsplit=1)[1]
                            break
                sub_category = doc_name.split(".")[0]
                if title:
                    doc_name = f"{doc_name}: {title}"
                if intra:
                    self.has_intranet = True
                    doc_name += Glossary.gettext("INTRANET_MARK")
            doc_href = Path("../../../..") / subdir

            self.struct.setdefault(man, {}).setdefault(sub_category, {})[
                doc_name
            ] = f"{doc_href}/index.html"

    def _filtered_docs(self):
        paths = sorted([i for i in SOURCE.glob("./*/man_*/*/*") if i.is_dir()])
        if self.pattern:
            if self.pattern.startswith("re:"):
                expr = re.compile(self.pattern.lstrip("re:"))
                func = expr.search
            else:
                func = lambda x: self.pattern in x
            paths = [i for i in paths if "other_pages" in str(i) or func(str(i.name))]
        return paths

    def sort_structure(self):
        """Sort and filter the documentation structure (in place)."""
        renumb = re.compile(r"^[0-9]+\.")
        new = {}
        for man, dsub in self.struct.items():
            new_dsub = {}
            for sub, docs in dsub.items():
                new_docs = {}
                for key in sorted(docs):
                    unnumb = renumb.sub("", key)
                    new_docs[unnumb] = docs[key]
                new_dsub[sub] = new_docs
            new_man = {}
            for sub in sorted(dsub.keys()):
                unnumb = renumb.sub("", sub)
                new_man[unnumb] = new_dsub[sub]
            new[man] = new_man
        self.struct = new

    def _build_one(self, man_path, size):
        """Build one document.

        Args:
            man_path (Path): Path to the document.
            i (int): Index of build.
            size (int): Total number of expected builds.

        Returns:
            tuple: A tuple containing the path to the ``objects.inv`` file and
            the objects it contains.
        """
        subdir = man_path.relative_to(SOURCE)
        i = counter.value + 1
        counter.value = i
        if size:
            prog = 100 * i // size
            print(f"+++ building {subdir.name:16s} {i:4d}/{size} {prog:3.0f}%", flush=True)
        else:
            print(f"+++ building {subdir.name:16s}", flush=True)
        logfile = self.cachedir / subdir / "build.log"
        sphinx_args = [
            "-b",
            self.format,
            "-c",
            str(SOURCE),
            "-d",
            str(self.cachedir / subdir),
            "-N",
            "-w",
            str(logfile),
            str(man_path),
            str(self.destdir / subdir),
        ]
        sphinx_args.extend(self.sphinx_args)
        retcode = sphinx.cmd.build.build_main(sphinx_args)
        objfile = self.destdir / subdir / "objects.inv"
        if self.symlinks:
            self._symlink_to_static(subdir)
        if self.remove_menu:
            remove_sidebar_menu((self.destdir / subdir).glob("**/*.html"))
        if retcode == 0:
            with open(logfile) as flog:
                retcode = int("ERROR:" in flog.read())
        if retcode != 0:
            msg = (
                f"build of {subdir.name} exits with code {retcode}."
                f" Try with options: -v --pattern={subdir.name}"
            )
            self.raise_error(msg)
            return None, None, msg
        return str(objfile), GlobalInventory.read(objfile), None

    def _symlink_to_static(self, subdir):
        """Add a symlink to the '_static' directory of the homepage."""
        static = self.destdir / subdir / "_static"
        if subdir.name == "home":
            if static.is_symlink():
                return
            if (self.destdir / "_static-shared").exists():
                shutil.rmtree(self.destdir / "_static-shared")
            if self.iframe:
                shutil.copy(
                    self.destdir / "_templates" / "dropdownmenu.html", static / "dropdownmenu.html"
                )
            shutil.copytree(static, self.destdir / "_static-shared")
        if static.is_symlink():
            static.unlink()
        elif static.is_dir():
            shutil.rmtree(static)
        os.symlink("../../../../_static-shared", static)

    def sphinx_build(self, jobs=1, nobuild=False):
        """Call `sphinx-build` on documents.

        Args:
            jobs (int): Number of jobs run in parallel (default: 1).
            nobuild (bool): Do not call *sphinx-build* if *True* (default: False).
        """
        ginv = GlobalInventory(self.destdir)
        os.environ["DOC_ASTER_BUILD_DIR"] = str(self.destdir)
        os.environ["DOC_ASTER_BASE_URL"] = self.baseurl
        docs = self._filtered_docs()
        size = len(docs)
        idx = Value("i", 0)
        arguments = [(path, size) for path in docs if "other_pages" in str(path) or not nobuild]

        with Pool(jobs, initializer=_pool_init, initargs=(idx,)) as pool:
            chunck = max(1, size // jobs)
            async_result = pool.starmap_async(self._build_one, arguments, chunksize=chunck)
            results = async_result.get()

        report = []
        for objfile, objects, error in results:
            if not error:
                ginv.append(objfile, objects)
            else:
                report.append(error)
        ginv.write(remove_duplicate=True)
        if report:
            print("ERROR:")
            for msg in report:
                print("-", msg)
            return 1
        return 0

    def generate_tpl(self):
        """Generate templates directory."""
        # top-bar menu
        tpldir = self.destdir / "_templates"
        tpldir.mkdir(exist_ok=True)
        for src in (SOURCE / "_templates").glob("*.html"):
            dst = tpldir / src.name
            if not dst.exists() or src.stat().st_mtime > dst.stat().st_mtime:
                shutil.copyfile(src, dst)
                shutil.copystat(src, dst)
        with open(DATA / "templates" / "menu.tpl") as f:
            menu_tpl = Template(f.read())
        categ = Glossary.gettext("CATEGORY_ORDER")
        menu_file = menu_tpl.render(category_order=categ, doc_structure=self.struct)
        if self.iframe:
            with open(DATA / "templates" / "includemenu.tpl") as f:
                update_file(tpldir / "menu.html", f.read())
            update_file(tpldir / "dropdownmenu.html", menu_file)
        else:
            update_file(tpldir / "menu.html", menu_file)
        # commands
        if "man_u" in self.struct:
            struct = sorted_commands(self.struct)
            self.generate_links_page(struct, "commands.html", keep_all=True)
        # test cases
        if "man_v" in self.struct:
            docs, dvalid, dverif = sorted_tests(self.struct)
            self.generate_links_page(docs, "test_cases.html", keep_all=True)
            self.generate_links_page(dvalid, "test_valid.html", keep_all=True)
            self.generate_links_page(dverif, "test_verif.html", keep_all=True)

    def generate_links_page(self, struct, html_filename, categories=None, keep_all=False):
        """Generate a special page containing links to `docs`.

        Args:
            docs (dict): Dict giving ``titles: hrefs``.
            html_filename (str): Path of the result html file.
            keep_all (bool): Disable filtering on manuals if *True*.
        """
        if not categories:
            categories = Glossary.gettext("CATEGORY_ORDER")
        with open(DATA / "templates" / "all_documents.tpl") as f:
            tpl = Template(f.read())
        content = tpl.render(
            category_order=categories,
            doc_structure=struct,
            unavailable=Glossary.gettext("UNAVAIL"),
            keep_all=keep_all,
        )
        update_file(self.cachedir / html_filename, content)

    def create_generic_docs(self):
        """Create some generic documents."""
        categ = Glossary.gettext("CATEGORY_ORDER")
        with open(DATA / "templates" / f"index.{self.lang}.tpl") as f:
            index_tpl = Template(f.read())
        index_content = index_tpl.render()
        update_file(self.destdir / "index.html", index_content)

        self.generate_links_page(self.struct, "all_documents.html")
        for man, name in categ.items():
            self.generate_links_page(self.struct, f"{man}.html", categories={man: name})

        with open(DATA / "templates" / f"home.{self.lang}.tpl") as f:
            home_tpl = Template(f.read())
        u0_00_01 = []
        dsub = self.struct.get("man_u", {}).get("u0", {})
        for title, href in dsub.items():
            if "u0.00.01" in title:
                u0_00_01 = title, href
                break
        home_content = home_tpl.render(
            category_order=categ,
            doc_structure=self.struct,
            u0_00_01=u0_00_01,
            date=time.strftime("%d/%m/%Y"),
            revision=os.environ.get("GITREV", "unknown")[:8],
            mark=Glossary.gettext("INTRANET_MARK") if self.has_intranet else "",
            intranet=self.has_intranet,
        )
        update_file(self.cachedir / "home.rst", home_content)

        with open(DATA / "templates" / "external_refs.tpl") as f:
            refs_tpl = Template(f.read())
        refs_content = refs_tpl.render(intranet=self.has_intranet)
        update_file(self.cachedir / "external_refs.rst", refs_content)

    def run(self, jobs=1, nobuild=False):
        """Build the html pages of the documentation.

        Args:
            jobs (int): Number of jobs run in parallel (default: 1).
            nobuild (bool): Do not call *sphinx-build* if *True*.
        """
        self.get_doc_structure()
        self.sort_structure()
        self.generate_tpl()
        self.create_generic_docs()
        return self.sphinx_build(jobs, nobuild)


class GlobalInventory:
    """Class to build a global objects inventory."""

    # should we sort the inventory as intersphinx does?
    # see https://github.com/sphinx-doc/sphinx/blob/master/sphinx/ext/intersphinx.py#L273

    project = "Documentation docAster"
    version = ""

    def __init__(self, builddir) -> None:
        self.builddir = Path(builddir)
        self.filename = self.builddir / "objects_global.inv"
        if self.filename.exists():
            self.globinv = SOI.Inventory(self.filename)
        else:
            self.globinv = SOI.Inventory()

    @staticmethod
    def read(objfile):
        """Read the objects from an inventory file.

        Args:
            objfile (Path): Inventory file.
        """
        objfile = Path(objfile)
        if not objfile.exists():
            return []
        return SOI.Inventory(objfile).objects

    def append(self, objfile, objects):
        """Append objects related to an object file.

        Args:
            objfile (Path|str): Inventory file.
            objects (list): List of DataObjects.
        """
        manpath = Path(objfile).parent.relative_to(self.builddir)
        globinv = self.globinv
        size = globinv.count
        if size == 0:
            globinv.project = GlobalInventory.project
            globinv.version = GlobalInventory.version

        for dobj in objects:
            dobj.uri = str(manpath / dobj.uri)
            globinv.objects.append(dobj)

    def remove_duplicate(self):
        """Remove duplicated entries"""
        globinv = self.globinv
        globinv.objects, previous = [], globinv.objects
        keys = set()
        for dobj in previous:
            if dobj.name in keys:
                continue
            keys.add(dobj.name)
            globinv.objects.append(dobj)

    def write(self, remove_duplicate=True):
        """Write global inventory file."""
        if remove_duplicate:
            self.remove_duplicate()
        ztext = SOI.compress(self.globinv.data_file())
        SOI.writebytes(self.filename, ztext)
        print(f"+++ {self.globinv.count} references written to global inventory file")


def load_mappings_wrapper(app) -> None:
    """Wrapper on `intersphinx.load_mappings` using a cache."""
    if hasattr(load_mappings_wrapper, "cache"):
        # use inventory cache
        (
            app.builder.env.intersphinx_cache,
            app.builder.env.intersphinx_inventory,
            app.builder.env.intersphinx_named_inventory,
        ) = load_mappings_wrapper.cache
        return

    _load_mappings_ref(app)
    load_mappings_wrapper.cache = (
        app.builder.env.intersphinx_cache,
        app.builder.env.intersphinx_inventory,
        app.builder.env.intersphinx_named_inventory,
    )


ISP.load_mappings = load_mappings_wrapper


def update_file(path, content):
    """Write text into a file if the file does not exist or if the content changed.

    Args:
        path (Path): Path to file.
        content (text): File content.
    """
    do_write = True
    if path.exists():
        with open(path) as fold:
            previous = fold.read()
        do_write = content != previous
        if do_write:
            print(f"+++ {path.name}: content changed")
        else:
            print(f"+++ {path.name}: cached")
    if do_write:
        path.write_text(content)


def sorted_commands(docs_struct):
    """Filter and sort documents related to commands.

    Args:
        docs_struct (dict): Dict containing the documents structure.

    Returns:
        dict: Dict filtering the documents structure for commands.
    """

    def _command_name(title):
        mat = re.search("([A-Z]{2}[A-Z_0-9]+)", title)
        if mat:
            return mat.group(1)

    new = []
    for sub, dsub in docs_struct.get("man_u", {}).items():
        if sub not in ("u4", "u7"):
            continue
        for title, link in dsub.items():
            if "u4.01" in title or "u4.03" in title:
                continue
            key = _command_name(title)
            if key:
                new.append([key, title, link])
            else:
                if "u4.51.11" in title:
                    continue
                raise ValueError(f"WARNING: Unexpected title : {title.strip()}")
    docs = {}
    for _, title, link in sorted(new):
        docs[title] = link
    struct = {"man_u": {Glossary.gettext("CMD_BY_NAME"): docs}}
    return struct


def sorted_tests(docs_struct):
    """Filter and sort documents related to testcases.

    Args:
        docs_struct (dict): Dict containing the documents structure.

    Returns:
        tuple[dict]: Dict filtering the documents structure for all testcases,
        for validation testcases and verification testcases.
    """

    def _test_name(title):
        mat = re.search("([A-Z]{3,5}[0-9]+)", title, re.I)
        if mat:
            return mat.group(1)

    with open(DATA / "list.validation") as fobj:
        valid = fobj.read().lower().split()

    subdirs = re.compile("^v[1-9]")
    new = []
    for sub, dsub in docs_struct.get("man_v", {}).items():
        if not subdirs.search(sub):
            continue
        for title, link in dsub.items():
            title_ = re.sub("<.*?>", "", title)
            key = _test_name(title_)
            if key:
                new.append([key, key.lower() in valid, title, link])
            else:
                raise ValueError(f"WARNING: Unexpected title : {title.strip()}")
    docs = {}
    dvalid = {}
    dverif = {}
    for _, isvalid, title, link in sorted(new):
        docs[title] = link
        if isvalid:
            dvalid[title] = link
        else:
            dverif[title] = link
    struct = {"man_v": {Glossary.gettext("TEST_BY_NAME"): docs}}
    struct_valid = {"man_v": {Glossary.gettext("TEST_VALID_BY_NAME"): dvalid}}
    struct_verif = {"man_v": {Glossary.gettext("TEST_VERIF_BY_NAME"): dverif}}
    return struct, struct_valid, struct_verif


def remove_sidebar_menu(paths):
    """Remove sidebar menu from all html files.

    Arguments:
        paths (list[Path]): List of filenames to be changed.
    """
    reduced = 0
    start = '<div class="sidebar-header-items sidebar-primary__section">'
    end = '<div class="sidebar-primary-items__start'
    e_start = re.escape(start)
    e_end = re.escape(end)
    expr = re.compile(f"{e_start}.*{e_end}", re.M | re.DOTALL)
    for page in paths:
        page = Path(page)
        if "_static" in page.parts:
            continue
        with page.open() as html:
            text = html.read()
        changed = expr.sub(f"{start}</div>\n{end}", text)
        size = len(text)
        with page.open("w") as html:
            html.write(changed)
        reduced += size - len(changed)


if __name__ == "__main__":
    args, others = parse_args()
    Glossary.select(args.lang)
    if "-q" not in others and "-v" not in others:
        others.append("-q")
    builder = Builder(args, others)
    sys.exit(builder.run(args.jobs, args.nobuild))
