
.. _%dockey%:

**%dockey%** Titre du document
================================

**Résumé :**

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut tempus quam.
Aliquam dolor risus, hendrerit sit amet urna ac, feugiat volutpat justo. Donec
eleifend tortor vitae felis lobortis, sollicitudin ultrices sem cursus. Quisque
vel arcu vel tellus fermentum semper pharetra in risus. Aenean vel risus eros.
Vestibulum tempor libero eu orci semper, nec mattis purus tempor. Mauris posuere
est in arcu dapibus, sit amet euismod risus tincidunt.

.. toctree::
    :hidden:

    self

.. toctree::
    :maxdepth: 2
    :numbered:

    page
