.. _%dockey%-page:

Titre de la page
================

**Remarque :**

    Ce fichier :file:`page.rst` doit être renommé pour correspondre au contenu
    de cette partie du document.


.. _%dockey%-section1:

Section 1
---------

Ceci est la première section.

.. _%dockey%-sous-section:

Sous-section
~~~~~~~~~~~~

Ceci est une sous-section.
