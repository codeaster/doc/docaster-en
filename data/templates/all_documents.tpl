<!DOCTYPE html>
<html>

<body>
    {% for section_key, section_name in category_order.items() %}
    {% if doc_structure.get(section_key, {}) %}
    <div id="liste"><h2>{{ section_name }}</h2></div>
    <ul>
        {% set ns = namespace(found=false) %}
        {% for doc_category, docs in doc_structure.get(section_key, {}).items() %}
        {% if keep_all or doc_category.replace("s", "")[0] in "arduvm" %}
        <li>
            {% set count = docs|length %}
            {% if count <= 1 %}
            <div id="{{ doc_category }}">{{ doc_category }} - {{ count }} document</div>
            {% else %}
            <div id="{{ doc_category }}">{{ doc_category }} - {{ count }} documents</div>
            {% endif %}
            <ul>
                {% for doc_name, doc_href in docs.items() %}
                {% set ns.found = true %}
                <li>
                    <a href="{{ doc_href }}">
                        {{doc_name}}
                    </a>
                </li>
                {% endfor %}
            </ul>
        </li>
        {% endif %}
        {% endfor %}
        {% if not ns.found %}
        <li>{{ unavailable }}</li>
        {% endif %}
    </ul>
    {% endif %}
    {% endfor %}
</body>

</html>
