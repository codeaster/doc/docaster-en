<ul class="bd-navbar-elements navbar-nav">
{% for section_key, section_name in category_order.items() %}
{% if doc_structure.get(section_key, {}) %}
<div class="dropdown">
  <button class="btn dropdown-toggle nav-item" type="button" id="dropdownMenuButton"
          data-bs-toggle="dropdown" aria-expanded="false">
    <b>{{ section_name }}</b>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    {% for doc_category, docs in doc_structure.get(section_key, {}).items() %}
    <li>
      {% if section_key not in ("man_a", "man_m") %}
      <a class="dropdown-item" href="../../../../manuals/{{ section_key }}/other_pages/list/index.html#{{doc_category}}">
      {% else %}
      <a class="dropdown-item" href="../../../../intranet/{{ section_key }}/other_pages/list/index.html#{{doc_category}}">
      {% endif %}
        {{doc_category}}
        <i class="dropdown-caret fa fa-caret-right" aria-hidden="true"></i>
      </a>
      <ul class="dropdown-menu dropdown-submenu">
        {% for doc_name, doc_href in docs.items() %}
        <li>
          <a class="dropdown-item" href="{{ doc_href }}">
            {{ doc_name }}
          </a>
        </li>
        {% endfor %}
      </ul>
    </li>
    {% endfor %}
  </ul>
</div>
{% endif %}
{% endfor %}
</ul>
