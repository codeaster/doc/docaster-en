{% if mark %}
.. _gitlab-codeaster: https://gitlab.pleiade.edf.fr/codeaster
.. _gitlab-codeaster/data: https://gitlab.pleiade.edf.fr/codeaster/data
.. _gitlab-codeaster/src: https://gitlab.pleiade.edf.fr/codeaster/src
.. _gitlab-codeaster/validation: https://gitlab.pleiade.edf.fr/codeaster/validation
.. _gitlab-codeaster/changelog: https://gitlab.pleiade.edf.fr/codeaster/changelog
.. _gitlab-codeaster/devtools: https://gitlab.pleiade.edf.fr/codeaster/devtools
.. _gitlab-codeaster/doc/docaster: https://gitlab.pleiade.edf.fr/codeaster/doc/docaster
.. _gitlab-codeaster/doc/docaster-intranet: https://gitlab.pleiade.edf.fr/codeaster/doc/docaster-intranet

.. _gitlab-salomemeca: https://gitlab.pleiade.edf.fr/salomemeca
.. _gitlab-salomemeca/modules: https://gitlab.pleiade.edf.fr/salomemeca/modules
.. _gitlab-salomemeca/tools: https://gitlab.pleiade.edf.fr/salomemeca/tools
.. _dépôts salome: https://gitlab.pleiade.edf.fr/salome
{% else %}
.. _gitlab-codeaster: https://gitlab.com/codeaster
.. _gitlab-codeaster/data: https://gitlab.com/codeaster/data
.. _gitlab-codeaster/src: https://gitlab.com/codeaster/src
.. _gitlab-codeaster/validation: https://gitlab.com/codeaster/validation
.. _gitlab-codeaster/changelog: https://gitlab.com/codeaster/changelog
.. _gitlab-codeaster/devtools: https://gitlab.com/codeaster/devtools
.. _gitlab-codeaster/doc/docaster: https://gitlab.com/codeaster/doc/docaster
.. _gitlab-codeaster/doc/docaster-intranet: https://gitlab.com/codeaster/doc/docaster-intranet

.. _gitlab-salomemeca: https://gitlab.com/salomemeca
.. _gitlab-salomemeca/modules: https://gitlab.com/salomemeca/modules
.. _gitlab-salomemeca/tools: https://gitlab.com/salomemeca
.. _dépôts salome: https://git.salome-platform.org/gitweb/
{% endif %}
