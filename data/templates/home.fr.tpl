La documentation est organisée en plusieurs manuels :

.. raw:: html

	<p><ul>
	    {% for section_key, section_name in category_order.items() %}
	    {% if doc_structure.get(section_key, {}) %}
        {% if section_key not in ("man_a", "man_m") %}
	    <li> <a href="../../../../manuals/{{ section_key }}/other_pages/list/index.html">{{ section_name }}</a></li>
	    {% else %}
	    <li> <a href="../../../../intranet/{{ section_key }}/other_pages/list/index.html">{{ section_name }}</a></li>
	    {% endif %}
	    {% endif %}
	    {% endfor %}
	</ul></p>

Les documents de chaque manuel sont accessibles par les menus déroulants du bandeau supérieur, sous-manuel par sous-manuel.

La liste de l'ensemble des documents est :ref:`accessible ici <list-all-documents>`.

{% if mark %}
.. raw:: html

    Les documents dont l'accès est restreint apparaissent avec le symbole «{{ mark }}» dans le menu.

{% endif %}

{% if u0_00_01 %}
Pour démarrer, consulter :ref:`u0.00.01`.
{% endif %}

.. raw:: html

    <p>À tout moment, cliquez sur l'icône
        <img src="_static/logo-light.png" height="25" class="logo__image only-light" alt_text="code_aster">
        <img src="_static/logo-dark.png" height="25" class="logo__image only-dark" alt_text="code_aster">
        dans l'entête pour revenir sur cette page.
    </p>

Contribuer à la documentation
    - Consulter ces deux documents :ref:`d8.00.00-presentation` et :ref:`d8.00.00-redaction`.
    - Créer ensuite un *Merge request* sur le dépôt `gitlab-codeaster/doc/docaster`_
      pour proposer vos modifications.
    {% if not intranet %}
    - Consulter également les instructions du
      fichier `CONTRIBUTING <https://gitlab.com/codeaster/doc/docaster/-/blob/main/CONTRIBUTING.md>`_.
    {% endif %}

Remarques
    - Cette documentation est issue d'une conversion depuis le format ODT.
      Sur les 25.000 pages converties, il subsiste quelques :ref:`erreurs connues de mises en forme <d8.00.01-conversion>`.
      Toutes les contributions pour les résorber sont les bienvenues.
    - Actuellement, la recherche [Ctrl-K] ne fonctionne que dans le document courant.

*Documentation construite le {{ date }} à la révision {{ revision }}.*

.. include:: ../../../../../_cache/external_refs.rst
