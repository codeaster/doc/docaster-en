The documentation is structured in different manuals:

.. raw:: html

	<p><ul>
	    {% for section_key, section_name in category_order.items() %}
	    {% if doc_structure.get(section_key, {}) %}
        {% if section_key not in ("man_a", "man_m") %}
	    <li> <a href="../../../../manuals/{{ section_key }}/other_pages/list/index.html">{{ section_name }}</a></li>
	    {% else %}
	    <li> <a href="../../../../intranet/{{ section_key }}/other_pages/list/index.html">{{ section_name }}</a></li>
	    {% endif %}
	    {% endif %}
	    {% endfor %}
	</ul></p>

The documents for each manual are available using the drop-down menus in the top banner, sub-manual by sub-manual.

The entire list of all the documents is :ref:`available here <list-all-documents>`.

{% if mark %}
.. raw:: html

    The documents with limited access are shown with this symbol «{{ mark }}» in the menu.

{% endif %}

{% if u0_00_01 %}
Getting started, see :ref:`u0.00.01`.
{% endif %}

.. raw:: html

    <p>Wherever you are, click on the icon
        <img src="_static/logo-light.png" height="25" class="logo__image only-light" alt_text="code_aster">
        <img src="_static/logo-dark.png" height="25" class="logo__image only-dark" alt_text="code_aster">
        in the header to return on this page.
    </p>

Contributing to the documentation
    - Read carefully these documents: :ref:`d8.00.00-presentation` and :ref:`d8.00.00-redaction`.
    - Then create a *Merge request* on the `gitlab-codeaster/doc/docaster`_ repository
      to submit your changes.
    {% if not intranet %}
    - Please read carefully the instructions from the
      `CONTRIBUTING <https://gitlab.com/codeaster/doc/docaster/-/blob/main/CONTRIBUTING.md>`_ file.
    {% endif %}

Notes
    - This documentation is the result of a conversion from the ODT format.
      From the 25.000 converted pages, there are still :ref:`some formatting errors <d8.00.01-conversion>`.
      We welcome any contributions you can make to help reduce them.
    - Currently, the search [Ctrl-K] only works in the current document.

*Documentation built at {{ date }} on revision {{ revision }}.*

.. include:: ../../../../../_cache/external_refs.rst
