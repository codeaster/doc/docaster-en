<!DOCTYPE html>
<html>
  <head>
  <title>code_aster documentation - redirect</title>

  <meta http-equiv="refresh" content="1; URL=manuals/man_u/other_pages/home/index.html">
  </head>

  <body>
    <p>
    Automatically redirected to
    <a href="manuals/man_u/other_pages/home/index.html">the homepage</a>.
    </p>
  </body>
</html>
